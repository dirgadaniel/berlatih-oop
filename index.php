 <?php
 require_once('Animal.php');
 require_once('Frog.php');
 require_once('Ape.php');


 $sheep = new Animal("shaun");
 echo "$sheep->name <br>";
 echo "$sheep->legs <br>";
 echo "$sheep->cold_blooded<br>";

 $frog = new Frog("buduk");
 echo "<br>$frog->name <br>";
 echo "$frog->legs <br>";
 echo "$frog->cold_blooded<br>";
 echo $frog->jump ();

 $sungokong = new Ape("Kera Sakti");
 echo "<br>$sungokong->name <br>";
 echo "$sungokong->legs <br>";
 echo "$sungokong->cold_blooded<br>";
 echo $sungokong->yell ();


  ?>
